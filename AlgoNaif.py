#!/usr/bin/env python2.7
# -*- coding: iso-8859-1 -*-
import time
import random
import sys,signal, os

#le chemin final
CheminMin=[]


def permutliste(n,er=False):
    """Retourne la liste de toutes les permutations de la liste seq (non r�cursif).
    si er=True: elimination des r�p�titions quand seq en a (ex: [1,2,2]
    o    """
    seq=range(0,n)
    p = [seq]
    for k in range(0,n-1):
        for i in range(0,len(p)):
            z = p[i][:]
            for c in range(0,n-k-1):
                z.append(z.pop(k))
                if er==False or (z not in p):
                    p.append(z[:])
    return p


def TourneeOptimale(n,w):
    """ la fonction principale qui calcule le chemin minimal """
    CheminMin=[]
    signal.signal(signal.SIGALRM,handler)
    signal.alarm(60)
    t = 10000000 
    
    for p in permutliste(n,er=False):
        s=0
        W=0
        # on parcourt toutes les permutations
        for i in range(0,n-2):
            s = s + w[p[i]][p[i+1]]
            W =  w[p[n-2]][n-1] +  w[n-1][p[0]] + s
            # mise � jour du poids t si W est inferieur � t 
        t = min(t,W)
        if t==W:
            # recuperer la nournee minimale
            CheminMin=list(p)
        #print w[p[n-2]][n-1] ," + ",  w[n-1][p[0]]
    print "chemin min = ", CheminMin
    return t
    signal.alarm(0)


def handler(signal,frame):
    print(" le temps d'attente est superieur a  60 secondes => quitter le programme \n")
    sys.exit(0)#quitter le programme

def banc_essai_naif():
    # banc d'essai pour tester l'algorithme avec plusieurs instances pour chaque n 
    n=5
    while True:
        print " --------------------------------------------"
        print "|   Pour n egal a  :  ",n, "                   |"
        print " --------------------------------------------"
        for g in range(1,11) :
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
            print "+ Tour numero :    ",g, "                   +"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
            current_dir = os.getcwd()# le dossier courant 
            c = current_dir+"/Test"
            nom1=str(n)+"_fic_"+str(g)+".txt"# creation du fichier selon n et l'instance
            
            fichier=open(c+"//"+nom1,"r")# ouverture du fichier
            
            w = fichier.read()# lecture du fichier
            liste=[]
            i = 0
            l=[]
            #convertir une liste de chaine de caracteres en int
            while i<len(w):
                a=""
                cpt =0
                j=i
                while w[j] !="," and w[j]!="\n":
                    a = a + w[j]
                    j = j + 1
                    cpt = cpt + 1
                if a not in ['']:
                    liste.append(int(a))
                i = i + cpt + 1
            k=0;
            # ww c'est la matrice recuperee apres la lecture du fichier
            ww=[]
            while k<len(liste):
                ligne=[]
                for j in range(n):
                    ligne.append(liste[k+j])
                ww.append(ligne)
                k = k + n
        
            start = time.time()
            TourneeOptimale(n,ww)# appeler la fonction principale
            end = time.time()
            print ('')
            print('Time elapsed: %s seconds' % (end - start))
            print('')
        n = n +5
        


#main
banc_essai_naif()

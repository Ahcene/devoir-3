#!/usr/bin/env python2.7
# -*- coding: iso-8859-1 -*-
import time
import signal ,os, sys
import random

# le chemin final
cheminMin=[]
coutCheminMin=10000



def majCheminMin(chemin,cout):
    """ mets � jour le chemin min et cout minimum """
    global coutCheminMin# variable globale
    if coutCheminMin>cout:
        global cheminMin
        cheminMin = list(chemin)
        coutCheminMin = cout
        
def coutChemin(w,chemin):
    #retourne la somme des couts des arcs reliant  les sommets du chemin indiqu�
    cout = 0
    for i in range(len(chemin)-1):
        cout = cout + w[chemin[i]][chemin[i+1]]
    return cout

def borne_inferieure(w,chemin, sommet, n):
    """ determine une borne inferieure pour le cout d'un circuit obtenu en ajoutant le sommet  indiqu� au chemin existant."""
    chemin1=list(chemin) # faire un copie du chemin pour ne pas modifier le chemin de depart
    chemin1.append(sommet)
    
    borne=0
    bornef=0    
    for i in range(0,n):
        appartient=0
        # min1 et min2 deux aretes du poids minimal
        min1=0
        min2=0
        l=[]
        min1=min(w[i])#recuperer l'arc du poids minimal
        if i in chemin1:# si le sommet i appartient au chemin
            if i==chemin1[0]:# si le sommet est le premier �l�ment du chemin
                min1=w[chemin1[0]][chemin1[1]]# on choisit l'arc qui relie le premier �l�ment et le deuxi�me �l�ment du chemin
            elif i==chemin1[len(chemin1)-1]:# si le sommet est le dernier �l�ment du chemin
                k = chemin1.index(i)#on r�cu�re  l'indice du sommet
                min1=w[chemin1[k-1]][chemin1[k]]#On choisit l'arc qui relie le d�rnier �l�ment et l'avant d�rnier �l�ment
            else:# sinon si le sommet se trouve au milieu, on choisit les deux arcs :  i et i+1, i et i-1
                appartient=1
                k = chemin1.index(i)
                min1=w[chemin1[k]][chemin1[k+1]] 
                min2=w[chemin1[k-1]][chemin1[k]]
        if appartient==0:# si le sommet i n'appartient pas au chemin             
            cpt = 0
            for k in w[i]:
                if k==min1:
                    cpt = cpt +1
                else:
                    l.append(k) 
            if cpt>1:# si le minimum se repete plusieurs fois
                min2 = min1# deux arcs avec un poids egal
            else:
                min2 = min(l)
            borne =  borne + min1 + min2
        else:#appartient==1
            borne =  borne + min1 + min2
    bornef=borne / 2.0 # la somme des bornes infs divis�e par 2
    return bornef


def dejaVisite(chemin, sommet):
    # retourn vrai si le sommet appartient au chemin
    return (sommet in chemin)

def ExplorerCircuits(w,chemin,n):
   
    if len(chemin)==n:
        cout = coutChemin(w,chemin) + w[chemin[len(chemin)-1]][chemin[0]]
        #On vient de trouver un chemin ayant  un cout inferieur
        majCheminMin(chemin,cout)    
    else: 
        #Certains sommets n'ont pas encore �t� visit�s
        #pour la position suivante, on explore, parmi tous les sommets n'ayant pas encore �t�  visit�s, uniquement  ceux qui semblent prometteurs.
        for s in range(n):
            if not  dejaVisite(chemin,s):
                borne = borne_inferieure(w,chemin,s,n)
                if borne < coutCheminMin :
                    # si il semble prometteur : on l'explore.
                    ch = list(chemin)
                    ch.append(s)
                    #appel recursif
                    ExplorerCircuits(w,ch,n)
                    
                    
def TrouverCircuitMin(w,sommetDepart,n):

    global cheminMin, coutCheminMin
    cheminMin = []# renitialiser le chemin min
    coutCheminMin = 10000
    signal.signal(signal.SIGALRM,handler)
    signal.alarm(60)# d�cloncher l'alarme
    chemin = []
    chemin.append(sommetDepart)# creer le chemin de debut on choisissant le sommet de depart au hasard
    print "chemin de depart ", chemin
    ExplorerCircuits(w,chemin,n)
    #On imprime le resultat
    print "chemin minimum  => ", cheminMin
    print " le cout du chemin minimum est => ", coutCheminMin
    signal.alarm(0)
    
def handler(signal,frame):
    print(" le temps d'attente est superieur a  60 secondes => quitter le programme \n")
    sys.exit(0)# quitter le programme



def banc_essai_branch():
    # banc d'essai pour tester l'algorithme avec plusieurs instances pour chaque n 
    n=5
    while True:
        nom=""
        print " --------------------------------------------"
        print "|   Pour n egal a  :  ",n, "                   |"
        print " --------------------------------------------"
        for g in range(1,11) :
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
            print "+ Tour numero :    ",g, "                   +"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
            current_dir = os.getcwd()# le dossier courant 
            c = current_dir+"/Test"
            nom1=str(n)+"_fic_"+str(g)+".txt"# creation du fichier selon n et l'instance
            
            fichier=open(c+"//"+nom1,"r")# ouverture du fichier
            
            w = fichier.read()# lecture du fichier
            liste=[]
            i = 0
            l=[]
            #convertir une liste de chaines de caracteres en int
            while i<len(w):
                a=""
                cpt =0
                j=i
                while w[j] !="," and w[j]!="\n":
                    a = a + w[j]
                    j = j + 1
                    cpt = cpt + 1
                if a not in ['']:
                    liste.append(int(a))
                i = i + cpt + 1
            k=0;
            # ww c'est la matrice recuperee apres la lecture du fichier
            ww=[]
            while k<len(liste):
                ligne=[]
                for j in range(n):
                    ligne.append(liste[k+j])
                ww.append(ligne)
                k = k + n
        
       
            
            sommetDepart=random.randint(0,n-1)# on choisit le sommet de d�part au hasard
            start = time.time()
            TrouverCircuitMin(ww,sommetDepart,n)# appeler la fonction principale
            end = time.time()
            print ('')
            print('Time elapsed: %s seconds' % (end - start))
            print('')
        n = n +5
        

#main
banc_essai_branch()



Ceci est un fichier texte nommé "readme.txt" contenant les explications des deux programmes Algorithme Naif et Algortithme branch and bound
répertoire d'installation, et peut être imprimés directement à partir de n'importe quel éditeur texte, une fois que les fichiers ont été déballés. 
============================================
    Developpeurs
============================================
Les deux programmes ont été dévéloppés par :
1- Ahcene Yousfi 
2- Mamadou Dian BARRY

============================================
    Generation des graphes
============================================
Une fonction qui genere nos graphes aleatoirement et qui stocke dans des fichiers pour permettre une comparaison nette des deux algorithmes.
Le dossier Test contient tous les fichiers des graphes et son nom se decompose comme suit : chiffre_fic_chiffre.txt
1er chiffre indique le nombre de noeud et le dernier chiffre indique le numero d'instance.
============================================
     Fonctionnement des deux algorithmes
============================================
Le probleme traité dans ces deux algorithmes est celui du voyageur de commerce en fournissant une tournée optimale
1-Algorithme Naif : il établit toutes les permutations possibles et retient celle qui fournit la tournée optimale (minimale) 
2-Algorithme branch and bound : L'objectif de cet algo est de déterminer le circuit de cout minimal tout en évitant d'explorer
des chemins qui ne sont pas prometteurs. Seul le circuit de cout minimum est identifié et affiché (procédure TrouverCircuitMin). Pour éviter l'exploration de certains chemins la fonction borne_inferieure d'estimation du cout mininmum est utilisée (Elaguer).

============================================
     Installation, Langage utilisé
============================================
Pour générer des graphes une fonction Generer_graphes est fournit et stocke tous les fichiers dans un dossier nommé test.
Attention : ne pas oublier de recuperer le dossier Test qui nous permet de tester nos deux algorithmes.
Sinon on peut tester les deux programmes directement avec nos fichiers deja générés.
Nous avons besoin des préréquis suivants:
	Pour utiliser les deux programmes , nous avons besoin d'installer une version Python 2.7 ou plus
	Pour exécuter nous avons besoin d'un systeme linux.
Ouvrir le terminal , allez dans le dossier contenant les deux programmes et lancez
 -python AlgoNaif.py  pour l'algo Naif
 -python branch-and_bound  pour l'algo branch and bound

============================================
     Les librairies utilisées
============================================ 
Toutes les librairies utilisées sont internes donc pas besoin d'installer des paquets 
La librairie time : utilisée pour nous fournir le temps nécessaire à l'exécution du programme.
la librairie random : qui nous permet de choisir nos poids (distance) aléatoirement.
la librairie sys : utilisée pour nous permettre de sortir de notre programme
la librairie siganl : qui envoie un signal à une fonction fermeture si le temps d'exécution 
librairie os :utilisée pour recuperer le dossier courant (traitement de fichiers)
de notre programme dépasse une minute
============================================
    Environnement de développement (IDE)
============================================
l'environnement de développemnt utilisé est un editeur de text Emacs , n'importe quel editeur pourrait marcher.

Si vous rencontrez des problemes d'exécutions ou de bugs, veillez nous envoyer vos
commentaires à l'equipe aux adresses suivantes : dianbarry35@gmail.com ou youahcene@gmail.com




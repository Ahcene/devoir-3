#!/usr/bin/env python2.7
# -*- coding: iso-8859-1 -*-

import random, os

def fonction_poids(n):
    """ fonction qui g�n�re la matrice de poids sym�trique � la diagonale """
    structure=[]
    for i in range(0,n):# remplir la matrice avec des z�ros
        ligne = []
        for j in range(0,n):
            ligne.append(0)    
        structure.append(ligne)
        
    for i in range(0,n):
        for j in range(0,n):
            if i==j:
                structure[i][j]=101
            else:            
                x = random.randint(1,100)# remplir la matrice al�atoirement
                structure[i][j]=x
                structure[j][i]=x
   
    return structure

def generer_fichier():
    n=5
    current_dir = os.getcwd()
    c = current_dir+"/Test"
    print c
    while n<=20:
        for g in range(1,11):
            w=fonction_poids(n)# g�n�rer la matrice de poids
            fichier = open(c+"//"+(str(n)+"_fic_"+str(g)+".txt"), "w")
            for k in range(len(w)):
                for i in w[k]:
                    fichier.write(str(i))
                    fichier.write(',')
                fichier.write("\n")
            fichier.close()
        n = n + 5

generer_fichier()
